<?php

class Test
{
	function positionReturn(String $argument_1 = '', int $argument_2 = 0)
	{
		$argument_1_array = [];
		$close_index_position = 0;

		for ($i=0; $i < strlen($argument_1) ; $i++) { 
			$argument_1_array[] = $argument_1[$i];
		}
		
		$open_index_position = $argument_2;
		$clear = 0;

		if ($argument_2 !== '(') {
			return 'error';			
		}

		foreach ($argument_1_array as $key => $row) {
			if ($key < $open_index_position) {
				continue;
			}

			if ($row === '(') {
				$clear ++;
			}

			if($row === ')') {
				$clear --;
			}

			if ($clear === 0) {
				$close_index_position = $key;
				break;
			}
		}

		return $close_index_position;
	}
}


$test = new Test();

$result = $test->positionReturn('a (b c (d e (f) g) h) i (j k)', 22);

print_r($result);