<?php
require __DIR__ . '/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class Test
{
	function validExcel($excel_file)
	{

		// Column name that starts with # should not contain any space
		// Column name that ends with * is a required column, means it must have a value
		// For each file type, it should validate the header columns name and the amount of columns it has:

		$input_type = \PhpOffice\PhpSpreadsheet\IOFactory::identify($excel_file);
		$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($input_type);
		$spreadsheet = $reader->load($excel_file);
		$content = $spreadsheet->getActiveSheet()->toArray();

		$header_rule = [];

		// header validate rule setup
		foreach($content as $key => $row) {      
			if ($key == 0) {
				foreach ($row as $key => $name) {
					if (!empty($name)) {
						$is_required = 0;
						$is_no_space = 0;

						if(strpos($name, '*') !== false){
							$is_required = 1;
						}

						if(strpos($name, '#') !== false){
							$is_no_space = 1;
						}

						$header_rule[] = [
							'name' => str_replace(['*', "#"], '', $name),
							'is_required' => $is_required,
							'is_no_space' => $is_no_space,
						];
					}
				}
			}
		}

		// validate
		$error = [];
		foreach($content as $key => $row) {      
			if ($key == 0) {
				continue; 
			}

			foreach ($row as $key_2 => $value) {
				if (isset($header_rule[$key_2])) {
					$current_header_rule = $header_rule[$key_2];
					if ($current_header_rule['is_required'] && empty($value)) {
						$error[$key + 1][] = 'Missing value in ' . $current_header_rule['name'];
					}

					if ($current_header_rule['is_no_space'] && preg_match('/^[^ ].* .*[^ ]$/', $value)) {
						$error[$key + 1][] = $current_header_rule['name'] . ' should not contain any space';
					}
				}
			}
		}

		return $error;
	}
}

$test_1 = new Test();
$excel_file = 'Type_A.xlsx';

$result = $test_1->validExcel($excel_file);

?>
	<table>
		<tr>
			<th>Row</th>
			<th>Error</th>
		</tr>
		<?php
			foreach ($result as $key => $row) {
		?>
			<tr>
				<td><?php echo $key ?></td>
				<td><?php echo implode(', ', $row) ?></td>
			</tr>
		<?php
			}
		?>
	</table>

<?php
echo '<br>';

$test_2 = new Test();
$excel_file = 'Type_B.xlsx';

$result = $test_1->validExcel($excel_file);

?>
	<table>
		<tr>
			<th>Row</th>
			<th>Error</th>
		</tr>
		<?php
			foreach ($result as $key => $row) {
		?>
			<tr>
				<td><?php echo $key ?></td>
				<td><?php echo implode(', ', $row) ?></td>
			</tr>
		<?php
			}
		?>
	</table>

